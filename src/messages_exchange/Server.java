package messages_exchange;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Работа по протоколу TCP
 * Сервер получает байт (число)
 * и возвращает клиенту
 */
@SuppressWarnings("InfiniteLoopStatement")
public class Server {
    private static final Logger LOG = Logger.getLogger(square.SqServer.class.getName());

    public static void main(String[] args) throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(1020)) {
            while (true) {
                Socket socket = serverSocket.accept();
                serveClient(socket);
            }
        }
    }

    private static void serveClient(Socket socket) throws IOException {
        LOG.info("Serving client" + socket.getInetAddress());

        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();

        BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(inputStream));
        String message = bufferedReader.readLine();
        LOG.info("Request:" + message);
        outputStream.write(message.getBytes());
        outputStream.flush();

    }
}
