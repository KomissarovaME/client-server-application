package messages_exchange;

import java.io.*;
import java.net.Socket;

public class Client {
    private static final String MESSAGE="Hello world!\n";
    public static void main(String[] args) throws IOException {
        try(Socket socket = new Socket("localhost", 1020)){
            OutputStream outputStream = socket.getOutputStream();

            outputStream.write(MESSAGE.getBytes());
            outputStream.flush();

            InputStream inputStream = socket.getInputStream();
            BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(inputStream));
            String message = bufferedReader.readLine();
            System.out.println("Request:" + message);
        }
    }
}
